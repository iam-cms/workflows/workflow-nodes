# workflow-nodes

**workflow-nodes** is a collection of various tools written in Python 3, which
are also usable inside a *workflow* as corresponding nodes. Each node is an
executable command line tool providing the `--xmlhelp` interface, which can be
used to obtain a machine readable representation of any command line tool and
its parameters (see also
[xmlhelpy](https://gitlab.com/iam-cms/workflows/xmlhelpy)). There are nodes for
many different tasks, including data conversion, transport and visualization
tools.

For installation and usage instructions, please see the documentation:

* Latest release: https://workflow-nodes.readthedocs.io/en/stable/
* Current state: https://workflow-nodes.readthedocs.io/en/latest/
