CLI
===

While the tools of this package were mainly created to be used within a *workflow*, all
tools can also be executed directly in the terminal using their corresponding command
line interface (CLI). This interface is also useful for scripting and the integration
with programming languages other than Python.

The first entry point to the CLI is given by running:

.. code-block:: shell

    workflow-nodes --help

.. click:: workflow_nodes.main:workflow_nodes
   :prog: workflow_nodes
   :nested: full
